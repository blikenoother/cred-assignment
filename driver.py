import sys
from tasks import main_task

def main(n):
	print(main_task(n))

if __name__ == "__main__":
	main(main_task(int(sys.argv[1])))