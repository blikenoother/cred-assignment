# README #

Sample code (to print 0 to n) in a distributed task scheduler infrascruture using python celery
[detail requirement](https://drive.google.com/file/d/0B1YVgoWCNfzXNlRBZkxYcFdTb1dSUm9CSkR2TFpPM2MtSmww/view?usp=sharing)

### Tools and Packages Used ###

* Python version=3.7+
* pip3 (any version)
* Redis-Server version=5.0.3+

### Wroker Setup ###
* clone this repository on all nodes `git clone git@bitbucket.org:blikenoother/cred-assignment.git`
* navigate to cloned repo and install libraries `pip3 install -r requirements.txt`
* make sure to create `config.json` with the keys mentioned in `config_template.json`
* and now run worker `celery -A tasks worker -concurrency=2 --loglevel=info`

### Trigger Main Task ###

* clone this repository `git clone git@bitbucket.org:blikenoother/cred-assignment.git`
* navigate to cloned repo and install libraries `pip3 install -r requirements.txt`
* make sure to create `config.json` with the keys mentioned in `config_template.json`
* run driver code to print 0 to n `python3 driver.py 1000` (here 1000 is n)
