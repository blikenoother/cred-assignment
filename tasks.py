from celery import Celery
from celery import group
import redis
import json
from exceptions import DuplicateTaskError

APP_CONFIG = None
with open('config.json') as f:
	APP_CONFIG = json.load(f)

celery_app = Celery(APP_CONFIG['CELERY_NAME'], broker=APP_CONFIG['CELERY_BROKER'], backend=APP_CONFIG['CELERY_BACKEND'])

redis_pool = redis.ConnectionPool(host=APP_CONFIG['DISTRIBUTED_LOCK_REDIS_HOST'], port=APP_CONFIG['DISTRIBUTED_LOCK_REDIS_PORT'],
									db=APP_CONFIG['DISTRIBUTED_LOCK_REDIS_DB'])
redis_con = redis.Redis(connection_pool=redis_pool)


@celery_app.task
def task(n):
	print(n)

@celery_app.task
def main_task(n):
	lock = redis_con.lock('main_task_{args}'.format(args=n))
	if lock.acquire(blocking=False):
		group([task.s(i) for i in range(0, n)]).apply_async()
		lock.release()
		return True
	else:
		raise DuplicateTaskError('task scheduler already running')
